# FROM golang:bullseye
FROM ubuntu:22.04

WORKDIR /go/src/app

ARG VCSIM_TYPE=vcsim_Linux_armv6

# prepare vcsim download
RUN apt update
RUN apt install jq wget golang-go curl -y

RUN wget $(curl -s https://api.github.com/repos/vmware/govmomi/releases/latest | jq -r ".assets[] | select(.name | test(\"$VCSIM_TYPE\")) | .browser_download_url")

# RUN wget $(echo $latesturl)
RUN tar -xzf vcsim_Linux_armv6.tar.gz
RUN rm vcsim_Linux_armv6.tar.gz

LABEL name="vCenter Appliance Simulator for ARMv6"
LABEL description="A VMware vCenter API mock server based on govmomi"
LABEL maintainer="cooling75"
LABEL version="0.1.0"

CMD ["./vcsim","-l","0.0.0.0:8989"]
